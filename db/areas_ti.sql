/*
Navicat PGSQL Data Transfer

Source Server         : local
Source Server Version : 100800
Source Host           : localhost:5432
Source Database       : eec
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 100800
File Encoding         : 65001

Date: 2019-10-15 16:42:52
*/


-- ----------------------------
-- Table structure for areas_ti
-- ----------------------------
DROP TABLE IF EXISTS "public"."areas_ti";
CREATE TABLE "public"."areas_ti" (
"id" int4 NOT NULL,
"nome" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of areas_ti
-- ----------------------------
INSERT INTO "public"."areas_ti" VALUES ('1', 'Dev (Frontend)');
INSERT INTO "public"."areas_ti" VALUES ('2', 'Dev (Backend)');
INSERT INTO "public"."areas_ti" VALUES ('3', 'Dev (Fullstack)');
INSERT INTO "public"."areas_ti" VALUES ('4', 'BI');
INSERT INTO "public"."areas_ti" VALUES ('5', 'AI');
INSERT INTO "public"."areas_ti" VALUES ('6', 'Big Data');
INSERT INTO "public"."areas_ti" VALUES ('7', 'Cloud');
INSERT INTO "public"."areas_ti" VALUES ('8', 'Analytics');
INSERT INTO "public"."areas_ti" VALUES ('9', 'Segurança');
INSERT INTO "public"."areas_ti" VALUES ('10', 'Infraestrutura');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table areas_ti
-- ----------------------------
ALTER TABLE "public"."areas_ti" ADD PRIMARY KEY ("id");
