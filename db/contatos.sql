-- Table: public.contatos

-- DROP TABLE public.contatos;

CREATE TABLE public.contatos
(
  contato json,
  data_hora_registro timestamp without time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.contatos
  OWNER TO postgres;