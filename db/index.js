const { Pool } = require('pg');
const keys = require('../config/keys');

const pool = new Pool({
	connectionString: keys.pgUri
});

pool.on('error', err => {
	console.error('An idle client has experienced an error', err.stack);
});

module.exports = {
	query: (text, params, callback) => {
		return pool.query(text, params, callback);
	}
};
