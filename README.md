# How to

## Dev
- Clonar projeto
- Rodar `npm install` na raiz
- Entrar na pasta 'client'
- Rodar `npm install` na pasta cliente
- Voltar para pasta raiz do projeto
- Rodar com `npm run dev`

## Deploy
- Logar no servidor
- Ir para a pasta do projeto
- Puxar última versão do projeto com `git pull origin master`
- Rodar o script `./simulador.sh`
- Verificar se o docker subiu com o comando `docker ps`

