import saveAs from 'file-saver';
import {
  EXPORT_CALCULADORA,
  EXPORT_PF,
  EXPORT_PJ,
  EXPORT_CONTATOS
} from '../actions/types';

const initialState = { loading: true };

export default function(state = initialState, action) {
  switch (action.type) {
    case EXPORT_CALCULADORA:
      // console.log(action);
      saveAs(
        new File([action.payload], { type: 'application/csv' }),
        'dados-calculadora.csv'
      );
      return {
        ...state,
        loading: false
      };
    case EXPORT_PF:
      var BOM = '\uFEFF';
      action.payload = BOM + action.payload;
      saveAs(
        new File([action.payload], {
          type: 'application/csv'
        }),
        'dados-contatos.csv'
      );
      return {
        ...state,
        loading: false
      };
    case EXPORT_PJ:
      saveAs(
        new File([action.payload], { type: 'application/csv' }),
        'dados-pessoa-juridica.csv'
      );
      return {
        ...state,
        loading: false
      };
    case EXPORT_CONTATOS:
      var BOM = '\uFEFF';
      action.payload = BOM + action.payload;
      saveAs(
        new File([action.payload], { type: 'application/csv' }),
        'dados-contato-site.csv'
      );
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
}
