import { combineReducers } from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import exportReducer from './exportReducer';

export default combineReducers({
	auth: authReducer,
	errors: errorReducer,
	export: exportReducer
});
