import axios from 'axios';

import {
	EXPORT_CALCULADORA,
	EXPORT_PF,
	EXPORT_PJ,
	EXPORT_CONTATOS
} from './types';

export const exportCalculadora = () => dispatch => {
	axios.get('/api/exportacao/exporta-calculadora').then(res =>
		dispatch({
			type: EXPORT_CALCULADORA,
			payload: res.data
		})
	);
};

export const exportPF = () => dispatch => {
	axios.get('/api/exportacao/exporta-contatos').then(res =>
		dispatch({
			type: EXPORT_PF,
			payload: res.data
		})
	);
};

export const exportPJ = () => dispatch => {
	axios.get('/api/exportacao/exporta-contatos-pj').then(res =>
		dispatch({
			type: EXPORT_PJ,
			payload: res.data
		})
	);
};

export const exportContatos = () => dispatch => {
	axios.get('/api/exportacao/exporta-contatos-site').then(res =>
		dispatch({
			type: EXPORT_CONTATOS,
			payload: res.data
		})
	);
};
