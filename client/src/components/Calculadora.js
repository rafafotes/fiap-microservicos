import 'materialize-css/dist/css/materialize.min.css';
import React from 'react';
import '../App.css';
import ConversationForm from './Conversation';
import DetectBrowser from 'react-detect-browser';

const Calculadora = () => {
  return (
    <div>
      <DetectBrowser>
        {({ browser }) =>
          browser ? (
            browser.name === 'firefox' && browser.version <= '60.0.0' ? (
              <div>
                <h1>Navegador não suportado</h1>
                <p>
                  {' '}
                  Utilize os links abaixo para baixar a última versão do seu
                  navegador preferido
                </p>
                <ul>
                  <li>
                    <a href="https://www.google.com/intl/pt-BR_ALL/chrome/">
                      Chrome
                    </a>
                  </li>
                  <li>
                    <a href="https://www.mozilla.org/pt-BR/firefox/new/">
                      Firefox
                    </a>
                  </li>
                  <li>
                    <a href="https://www.microsoft.com/pt-br/windows/microsoft-edge">
                      Edge
                    </a>
                  </li>
                  <li>
                    <a href="https://www.opera.com/pt-br/download">Opera</a>
                  </li>
                </ul>
              </div>
            ) : (
              <ConversationForm />
            )
          ) : (
            <div>
              <h1>Navegador não suportado..</h1>
              <p>
                Utilize os links abaixo para baixar a última versão do seu
                navegador preferido
              </p>
              <ul>
                <li>
                  <a href="https://www.google.com/intl/pt-BR_ALL/chrome/">
                    Chrome
                  </a>
                </li>
                <li>
                  <a href="https://www.mozilla.org/pt-BR/firefox/new/">
                    Firefox
                  </a>
                </li>
                <li>
                  <a href="https://www.microsoft.com/pt-br/windows/microsoft-edge">
                    Edge
                  </a>
                </li>
                <li>
                  <a href="https://www.opera.com/pt-br/download">Opera</a>
                </li>
              </ul>
            </div>
          )
        }
      </DetectBrowser>
    </div>
  );
};

export default Calculadora;
