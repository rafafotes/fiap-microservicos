import "materialize-css/dist/css/materialize.min.css";
import M from "materialize-css/dist/js/materialize.min.js";
//import cf from 'conversational-form';
import React, { Component } from "react";
import "../App.css";
import { BrowserRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../store";

import Calculadora from "./Calculadora";
import NavBar from "./layout/Navbar";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false
    };
  }

  componentDidMount() {
    console.log("COMPONENT HAS MOUNTED");
    M.AutoInit();
    // this.toggleAuthenticateStatus();
  }

  render() {
    return (
      <Provider store={store}>
        <BrowserRouter basename={"/"}>
          <div>
            <NavBar />
            <Route path="/chat" component={Calculadora} />
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
