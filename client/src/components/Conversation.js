import React, { Component } from 'react';
import cf from 'conversational-form';
import icone from '../images/fiap_pequeno.jpg';
import smile from '../images/Wink_Emoji.png';
import '../cf.css';
// import promise from 'es6-promise';
import 'isomorphic-fetch';

class ConversationForm extends Component {
	constructor(props) {
		super(props);
		this.cf = null; // <-- Conversational Form ref
		this.state = {
			areas: [],
			areasEscolhida: '',
			dadosConsumo: {},
			imagemConta: '',
			matricula: ''
		};
	}

	componentDidMount() {
		

		console.log(cf);

		let initAreas = [];
		fetch('/api/chatbot/areas-piloto')
			.then(response => {
				return response.json();
			})
			.then(data => {
				
				initAreas = data.map(areas => {
					return {
						tag: 'option',
						'cf-label': areas.nome,
						value: areas.nome
					};
				});
				initAreas.push({
					tag: 'option',
					'cf-label': 'Outra área',
					value: 'ND'
				});
				
				this.setState({
					areas: initAreas
				});
			});

		

		let that = this;
		this.cf = new cf.startTheConversation({
			formEl: this.refs.form,
			context: document.getElementById('cf-context'),
			userImage: 'https://gulpjs.com/img/gulp-whi	te-text.svg',
			robotImage: icone,
			suppressLog: false,
			
			dictionaryData: {
				'entry-not-found': 'Opção não disponível.',
				'input-placeholder': 'Responder...',
				'input-placeholder-required':
					'Resposta necessária para continuar ...',
				'input-placeholder-error': 'Resposta inválida ...',
				'input-placeholder-file-error': 'Erro no upload do arquivo ...',
				'input-placeholder-file-size-error': 'Arquivo muito grande ...',
				'input-no-filter':
					'Nenhum resultado encontrado para {input-value}',
				'user-reponse-and': ' e ',
				'user-reponse-missing': 'Sem resposta ...',
				'group-placeholder': 'Digite para filtrar a lista ...',
				general: 'Geral Typ1|Geral Typ2'
			},

			flowStepCallback: function(dto, success, error) {
				if (dto.tag.id === 'matricula') {
					this.cfReference.addTags([
						{
							tag: 'select',
							name: 'areas',
							'cf-questions':
								'Olá {nome}, com quais dos temas abaixo você trabalha? ',
							'cf-input-placeholder':
								'Escreva as primeiras letras e escolha na lista.',
							id: 'areas',
							ref: 'areas',
							children: initAreas
						}
					]);
				}
				if (dto.tag.id === 'areas') {
					if (dto.tag.value[0] === 'ND') {
						this.cfReference.addTags([
							{
								tag: 'input',
								type: 'text',
								name: 'areas-input',
								'cf-questions':
									'Não encontrou na lista? Não tem problema, informe aqui.',
								id: 'areas-input',
								ref: 'areas-input'
							},
						]);
						success();
					}
					success();
				}
				if (dto.tag.id === 'upload-carta') {
					console.log(dto);
					let allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
					if (typeof that.refs.uploadcarta.files[0] !== 'undefined') {
						// var maxSize = parseInt($(this).attr('max-size'),10),
						let size = that.refs.uploadcarta.files[0].size;
						console.log('passou aqui e o arquivo é ', size);
						if (size > 5000000) {
							error('Arquivo deve ser menor que 3Mb.');
						} else if (!allowedExtensions.exec(dto.tag.value)) {
							console.log(dto.text);
							error('Tipo de arquivo inválido');
						} else {
							success('Upload realizado com sucesso!');
						}
					} else {
						error('Clique no botão ao lado para enviar arquivo.');
					}
				}
				if (
					![
						'areas',
						'upload-conta'
					].includes(dto.tag.id)
				) {
					success();
				}
			},
			submitCallback: function() {
				
				const fd = new FormData();
				this.tags.forEach(e => {
					fd.append(e.name, e.value);
				});

				if (that.refs.uploadcarta.files[0]) {
					fd.append(
						'image',
						that.refs.uploadcarta.files[0],
						that.refs.uploadcarta.files[0].name
					);
					for (var key of fd.entries()) {
						console.log(key[0] + ', ' + key[1]);
					}
				}

				console.log('Form submitted...');
				var request = new Request('/api/chatbot/guarda-contato', {
					method: 'POST',
					body: fd
				});

				fetch(request)
					.then(function(response) {
						response.json().then(function(data) {});
					})
					.catch(function(err) {
						console.log(err);
					});
			}
		});

	}

	render() {
		return (
			<div>
				<h2>{this.props.titulo}</h2>

				<div
					style={{
						position: 'relative',
						float: 'left',
						visibility: 'hidden'
					}}
				>
					<form id="form" className="form" ref="form" style={{}}>
						<input
							type="text"
							name="nome"
							defaultValue={this.props.name}
							id="nome"
							cf-questions="Para começarmos, como posso te chamar?"
						/>

						<input
							type="text"
							pattern="^$|[Rr]{1}[Mm]{1}[0-9]{6}"
							maxLength="8"
							id="matricula"
							name="matricula"
							cf-error="Informe a matrícula corretamente."
							cf-input-placeholder="RMXXXXXX"
							cf-questions="Você pode nos informar a sua matrícula?"
						/>

						<input
							type="text"
							ref="tempoArea"
							name="tempoArea"
							id="tempoArea"
							// cf-conditional-areas="^(?!ND$).*$"
							cf-input-placeholder="Ex.: 10 anos"
							cf-questions="A quanto tempo trabalha na área? "
						/>
						<input
							type="text"
							name="descricao"
							id="descricao"
							cf-questions="Faça uma breve descrição dos seus conhecimentos. Ferramentas que utiliza, metodologias que adota, cursos que realizou, etc."
							required
						/>
						<cf-robot-message
							cf-questions="Muito bom. Já dá pra começar a traçar seu perfil profissional."
						/>
						<cf-robot-message
							cf-questions="Vamos continuar."
						/>

						<fieldset cf-questions="Qual seu conhecimento do idioma inglês?">
							<label htmlFor="intro-yes">
								Avançado
								<input
									type="radio"
									name="conhece-ingles"
									ref="conhece-ingles"
									id="conhece-ingles-avancado"
									value="avancado"
									
								/>
							</label>

							<label htmlFor="intro-no">
								Intermediário
								<input
									type="radio"
									name="conhece-ingles"
									ref="conhece-ingles"
									id="conhece-ingles-intermediario"
									value="intermediario"
									
								/>
							</label>
							<label htmlFor="intro-no">
								Básico
								<input
									type="radio"
									name="conhece-ingles"
									ref="conhece-ingles"
									id="conhece-ingles-basico"
									value="basico"
									
								/>
							</label>
							<label htmlFor="intro-no">
								Nenhum
								<input
									type="radio"
									name="conhece-ingles"
									ref="conhece-ingles"
									id="conhece-ingles-nenhum"
									value="nenhum"
									
								/>
							</label>
						</fieldset>
						<cf-robot-message
							cf-questions="Oh my god! Recomendo muito procurar um curso. Inglês é fundamental na área de TI. The life snake (a vida cobra)"
							cf-conditional-conhece-ingles="nenhum"
						/>
						

						<fieldset cf-questions="Qual seu conhecimento do idioma Espanhol?">
							<label htmlFor="intro-yes">
								Avançado
								<input
									type="radio"
									name="conhece-espanhol"
									ref="conhece-espanhol"
									id="conhece-espanhol-avancado"
									value="avancado"
									
								/>
							</label>

							<label htmlFor="intro-no">
								Intermediário
								<input
									type="radio"
									name="conhece-espanhol"
									ref="conhece-espanhol"
									id="conhece-espanhol-intermediario"
									value="intermediario"
									
								/>
							</label>
							<label htmlFor="intro-no">
								Básico
								<input
									type="radio"
									name="conhece-espanhol"
									ref="conhece-espanhol"
									id="conhece-espanhol-basico"
									value="basico"
									
								/>
							</label>
							<label htmlFor="intro-no">
								Nenhum
								<input
									type="radio"
									name="conhece-espanhol"
									ref="conhece-espanhol"
									id="conhece-espanhol-nenhum"
									value="nenhum"
									
								/>
							</label>
						</fieldset>
						<cf-robot-message
							cf-questions="Ay caramba! A américa latina é um mercado imenso e tem inúmeras oportunidades. Vale a pena investir nesse conhecimento."
							cf-conditional-conhece-espanhol="nenhum"
						/>
						<fieldset
							cf-questions="Pretende aprender tecnologias nova ou se atualizar das que já conhece no próximo ano?"
						>
							<label htmlFor="bateu-sim">
								Sim
								<input
									type="radio"
									name="novo-conheciemnto"
									id="novo-conheciemnto-sim"
									value="sim"
								/>
							</label>

							<label htmlFor="bateu-nao">
								Não
								<input
									type="radio"
									name="novo-conheciemnto"
									id="novo-conheciemnto-nao"
									value="nao"
								/>
							</label>
						</fieldset>
						<input
							type="text"
							name="quais-conhecimentos"
							id="quais-conhecimentos"
							cf-conditional-novo-conheciemnto="sim"
							cf-questions="Quais?"
						/>
						<input
							type="text"
							name="ambiente"
							id="ambiente"
							cf-questions="Na sua opinião, qual o ambiente de trabalho ideal?"
						/>
						<fieldset cf-questions="Deseja enviar carta de recomendação?">
							<label htmlFor="upload-carta-sim">
								Sim
								<input
									type="radio"
									name="upload-carta"
									id="upload-sim"
									value="sim"
								/>
							</label>

							<label htmlFor="upload-carta-nao">
								Não
								<input
									type="radio"
									name="upload-carta"
									id="upload-nao"
									value="nao"
								/>
							</label>
						</fieldset>
						<input
							type="file"
							id="upload-carta"
							name="uploadcarta"
							ref="uploadcarta"
							cf-questions="Para enviar, carregue o arquivo no botão abaixo:"
							cf-conditional-upload-conta="sim"
							accept="image/x-png, image/gif, image/jpegs, application/pdf"
							data-max-size="10000"
						/>

						<input
							type="text"
							name="comentario"
							id="comentario"
							cf-questions="Terminamos! Quer deixar algum comentário, dúvida ou crítica? "
						/>
						<cf-robot-message cf-questions="Muito obrigado {nome}! Você nos ajudou muito!" />
					</form>
				</div>
				<div
					id="cf-context"
					style={{
						height: '89vh',
						maxWidth: 1000,
						marginLeft: 'auto',
						marginRight: 'auto'
					}}
				>
					{' '}
					{/* <-- the cf form will be bound to this element */}
				</div>
			</div>
		);
	}
}

export default ConversationForm;
