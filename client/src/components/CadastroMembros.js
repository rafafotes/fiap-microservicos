import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css/dist/js/materialize.min.js';
//import cf from 'conversational-form';
import React, { Component } from 'react';

class CadastroMembros extends Component {

  componentDidMount() {
    console.log('COMPONENT HAS MOUNTED');
    M.AutoInit();
  }

  render() {
    // console.log(estados);
    // console.log(optionEstados);
    // onChange={(e)=>{this.setState({comboEstado: e.target.value})}}

    return (
      <div className="row">
        <div className="col s12 m4 offset-m4">
          <div className="card">
            <div className="card-action green darken-1 white-text">
              <h3>Cadastro</h3>
            </div>
            <form action="/api/cadastra-membro" method="post">
              <div className="card-content">
                <div className="input-field">
                  <label htmlFor="username">Usuário</label>
                  <input type="text" name="username"/>
                </div>
                <br/>
                <div className="input-field">
                  <label htmlFor="password">Senha</label>
                  <input type="password" name="password" />
                </div>
                <br/>
                
                <div className="form-field">
                  <button className="btn-large waves-effect waves-dark green darken-1" style={{width: '100%'}}>Cadastrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CadastroMembros;
