import "materialize-css/dist/css/materialize.min.css";
import React from "react";
import logo from "../images/fiap.png";
import "../App.css";

const Header = () => {
  return (
    <div>
      <nav>
        <div className="nav-wrapper white">
          <a href="/" className="brand-logo left">
            <img
              src={logo}
              style={{ width: "250px", marginLeft: "10px" }}
              alt=""
            />{" "}
          </a>
          <ul className="right hide-on-med-and-down text-black">
            <li>
              <a href="/" className="light-green-text text-darken-1">
                <i className="material-icons">person_pin</i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default Header;
