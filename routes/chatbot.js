const express = require("express");
const router = express.Router();
const db = require("../db");

router.get("/test", (req, res) => res.json({ msg: "Posts works" }));

router.get("/areas-piloto", function(req, res) {
  db.query(
    "SELECT DISTINCT nome FROM public.areas_ti ORDER BY 1",
    (err, table) => {
      if (err) {
        console.log("Erro ao buscar áreas TI:", err);
        return res.status(400).send(err);
      } else {
        console.log("Sucesso ao buscar áreas de TI.");
        return res.status(200).send(table.rows);
      }
    }
  );
});

router.post("/guarda-contato", function(req, res) {
  if (req.files) {
    let imageFile = req.files.image;
    imageFile.mv(
      `${__dirname}/client/src/images/contas/${imageFile.name}`,
      function(err) {
        if (err) {
          console.log("Erro ao salvar imagem");
          return res.status(500).send(err);
        }
        console.log("Dados arquivo", err);
        // res.json({file: `public/${imageFile.name}`});
      }
    );
  }

  db.query(
    "INSERT INTO public.contatos (contato, data_hora_registro) VALUES ($1, now())",
    [req.body],
    (err, table) => {
      if (err) {
        return res.status(400).send(err);
      } else {
        console.log("DATA INSERTED");
        res.status(201).send({ message: "Data Inserted!" });
      }
    }
  );
});

module.exports = router;
