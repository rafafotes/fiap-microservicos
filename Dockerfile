FROM node:8

# Override the base log level (info).
ENV NPM_CONFIG_LOGLEVEL warn
ENV NODE_ENV production

# Install and configure `serve`.
#RUN npm install -g serve
#CMD serve -s build
EXPOSE 5000:5000

# Install all dependencies of the current project.
COPY package.json package.json
RUN npm install

COPY . .

WORKDIR ./client
RUN pwd
RUN df -h

RUN npm install
COPY conversational-form.js node_modules/conversational-form/dist/conversational-form.js

# Copy all local files into the image.

# Build for production.
RUN npm run build 

WORKDIR ..

RUN pwd

CMD npm start
